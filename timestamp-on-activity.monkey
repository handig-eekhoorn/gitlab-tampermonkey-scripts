// ==UserScript==
// @match        https://gitlab.com/*/activity
// @name         GitLab Timestamp on Activity List
// @namespace    https://gitab.com/handig-eekoorn/gitlab-tampermonkey-scripts/-/blob/main/timestamp-on-activity.monkey
// @version      0.1
// @author       Hannes Erven
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

      // delay required to have related issues load first
  window.setInterval(main, 30000);

  function main() {
    var list = $("[class=event-item-timestamp] time");
    list.each(idx => {var y = list[idx]; y.textContent = y.getAttribute("datetime") });
  }

})();
