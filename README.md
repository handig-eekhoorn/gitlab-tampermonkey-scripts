# Gitlab-Tampermonkey-Scripts

This repository contains some [Tampermonkey](https://www.tampermonkey.net/) scripts
to customize Gitlab's UI.


(C) [Handig Eekhoorn GmbH](https://www.handig-eekhoorn.at/), [CC BY](https://creativecommons.org/licenses/by/)
